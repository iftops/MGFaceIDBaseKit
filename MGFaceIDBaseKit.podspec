Pod::Spec.new do |s|  
  s.name              = 'MGFaceIDBaseKit'
  s.version           = '1.0.0'
  s.summary           = 'MGFaceIDBaseKit.'
  s.homepage          = 'https://gitee.com/iftops/MGFaceIDBaseKit'

  s.author            = { 'Name' => 'sdk@example.com' }
  s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

  s.platform          = :ios
  s.source            = { :git => "https://gitee.com/iftops/MGFaceIDBaseKit.git", :tag => "#{s.version}" }

  s.ios.deployment_target = '8.0'
  s.ios.vendored_frameworks = 'MGFaceIDBaseKit.framework'
end